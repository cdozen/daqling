#
# external library versions
#

set(spdlog_version                   1.3.1)
set(cereal_version                   1.2.2)
set(evpp_version                     0.7.0)
set(docopt_version                   0.6.2)
set(json_version                     3.5.0)
set(cassandra-driver_version         2.11.0)

#
# For find_ modules
#
set(tbb_version                      $ENV{TBB_VERSION})
set(TBB_ROOT_DIR                     $ENV{TBB_ROOT_DIR})
set(boost_version                    $ENV{BOOST_VERSION})
set(BOOST_ROOT                       $ENV{BOOST_ROOT_DIR})

